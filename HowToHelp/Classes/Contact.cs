﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HowToHelp.Classes
{
    [Table]
    public class Contact
    {
        [Column(IsDbGenerated=true, IsPrimaryKey=true)]
        public int idContact { get; set; }
        [Column]
        public String name { get; set; }
        [Column]
        public String phone { get; set; }
        [Column]
        public int idUser { get; set; }
    }
}
