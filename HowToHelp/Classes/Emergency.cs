﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HowToHelp.Classes
{
    [Table]
    public class Emergency
    {
        [Column(IsDbGenerated=true, IsPrimaryKey=true)]
        public int idEmergency { get; set; }
        [Column]
        public String title { get; set; }
        [Column]
        public String description { get; set; }
        [Column]
        public String condition { get; set; }
        [Column]
        public String location { get; set; }
        [Column]
        public int idUser { get; set; }
    }
}
