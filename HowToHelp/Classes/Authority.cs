﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HowToHelp.Classes
{
    [Table]
    public class Authority
    {
        [Column(IsDbGenerated=true, IsPrimaryKey=true)]
        public int idAuthority { get; set; }
        [Column]
        public String name { get; set; }
        [Column]
        public String phoneNumber { get; set; }
    }
}
