﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HowToHelp.Classes
{
    [Table]
    public class Proceeding
    {
        [Column(IsDbGenerated=true, IsPrimaryKey=true)]
        public int idProceeding {get; set;}
      /*  [Column]
        public String title { get; set; }*/
        [Column]
        public String description { get; set; }
        [Column]
        public int idDisaster { get; set; }
        [Column]
        public Boolean isChecked { get; set; }
    }
}
