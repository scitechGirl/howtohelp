﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HowToHelp.Classes
{
    [Table]
    public class User
    {
        [Column(IsDbGenerated=true, IsPrimaryKey=true)]
        public int idUser { get; set; }
        [Column]
        public String name { get; set; }
        [Column]
        public String phoneNumber { get; set; }
        [Column]
        public String city { get; set; }
    }
}
