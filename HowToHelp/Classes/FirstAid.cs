﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HowToHelp.Classes
{
    [Table]
    public class FirstAid
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true)]
        public int idFirstAid { get; set; }
        [Column]
        public String title { get; set; }
    }
}
