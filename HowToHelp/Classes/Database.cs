﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HowToHelp.Classes
{
    public class Database : DataContext
    {
        public static Database Instance { get; private set; }

        static Database()
        {
            Instance = new Database();
        }

        public Table<Emergency> Emergencies;
        public Table<User> Users;
        public Table<Authority> Authorities;
        public Table<Proceeding> Proceedings;
        public Table<Disaster> Disasters;
        public Table<Contact> Contacts;
        public Table<ProceedingFirstAid> ProceedingFirstAids;
        public Table<FirstAid> FirstAids;

        private Database()
            : base("Data Source=isostore:/database.sdf")
        {
            if (DatabaseExists() == false)
            {
                CreateDatabase();

                /*var user = new User { name = "admin", phoneNumber = "555-555" };
                this.Insert<User>(user);*/

                /* DISASTERS */

                var dis1 = new Disaster { title = "Flood" };
                this.Insert<Disaster>(dis1);

                var dis2 = new Disaster { title = "Hurricane" };
                this.Insert<Disaster>(dis2);

                var dis3 = new Disaster { title = "Earthquake" };
                this.Insert<Disaster>(dis3);

                var dis4 = new Disaster { title = "Snowstorm" };
                this.Insert<Disaster>(dis4);

                /* PROCEEDINGS */

                /*Procedimentos de enchente*/
                var pro1 = new Proceeding { description = "If your house is in a risky location, it is recommended to leave the place.", idDisaster = 1 };
                this.Insert<Proceeding>(pro1);

                var pro2 = new Proceeding { description = "Keep your doccuments in a safe place. They can be necessary.", idDisaster = 1 };
                this.Insert<Proceeding>(pro2);

                var pro3 = new Proceeding { description = "If you have time, take this basic itens with you: water, food and dry clothes.", idDisaster = 1 };
                this.Insert<Proceeding>(pro3);

                var pro4 = new Proceeding { description = "Go to a high place, where the water cannot reach.", idDisaster = 1 };
                this.Insert<Proceeding>(pro4);

                var pro5 = new Proceeding { description = "Remember: your life is more important than your belongins.", idDisaster = 1 };
                this.Insert<Proceeding>(pro5);

                var pro6 = new Proceeding { description = "Always have a flashlight with you (or you can use your phone’s flashlight).", idDisaster = 1 };
                this.Insert<Proceeding>(pro6);

                var pro7 = new Proceeding { description = "Turn off the electricity on the power box.", idDisaster = 1 };
                this.Insert<Proceeding>(pro7);

                /*Procedimentos de furacão*/
                var pro8 = new Proceeding { description = "Prepare before you hear about a hurricane. Plan in place and putting together an Emergency Disaster Supply Kit that is easy for all family members to locate in a hurry.", idDisaster = 2 };
                this.Insert<Proceeding>(pro8);

                var pro9 = new Proceeding { description = "Plan an emergency evacuation route.", idDisaster = 2 };
                this.Insert<Proceeding>(pro9);

                var pro10 = new Proceeding { description = "Make sure even the youngest know how to call 911.", idDisaster = 2 };
                this.Insert<Proceeding>(pro10);

                var pro11 = new Proceeding { description = "Buy a generator.", idDisaster = 2 };
                this.Insert<Proceeding>(pro11);

                var pro12 = new Proceeding { description = "Purchase self-powered radios and self-powered flashlights batteries.", idDisaster = 2 };
                this.Insert<Proceeding>(pro12);

                var pro13 = new Proceeding { description = "Secure your property.", idDisaster = 2 };
                this.Insert<Proceeding>(pro13);

                var pro14 = new Proceeding { description = "Dispose of or eat all perishable foods 12 hours before the storm.", idDisaster = 2 };
                this.Insert<Proceeding>(pro14);

                var pro15 = new Proceeding { description = "Make sure that anything you normally leave outside is either tied down or brought inside 6 hours before the storm.", idDisaster = 2 };
                this.Insert<Proceeding>(pro15);

                var pro16 = new Proceeding { description = "Evacuate, if you can. Go somewhere north of the storm, where it will have lost strength by the time it reaches the area.", idDisaster = 2 };
                this.Insert<Proceeding>(pro16);

                var pro17 = new Proceeding { description = "Bring your family and pets into the shelter 2 hours before the storm.", idDisaster = 2 };
                this.Insert<Proceeding>(pro17);

                var pro18 = new Proceeding { description = "Stay away from windows, skylights and glass doors during the hurricane.", idDisaster = 2 };
                this.Insert<Proceeding>(pro18);

                var pro19 = new Proceeding { description = "Lie on the floor under something sturdy, such as a table.", idDisaster = 2 };
                this.Insert<Proceeding>(pro19);

                /* Procedimentos de terremoto */

                var pro26 = new Proceeding { description = "Cover your head and neck. Use your hands and arms to protect these vital areas from falling objects. ", idDisaster = 3 };
                this.Insert<Proceeding>(pro26);

                var pro27 = new Proceeding { description = "If you are in a building, slowly and carefully leave.", idDisaster = 3 };
                this.Insert<Proceeding>(pro27);

                var pro28 = new Proceeding { description = "Inspect your house for anything that might be in a dangerous condition.", idDisaster = 3 };
                this.Insert<Proceeding>(pro28);

                var pro29 = new Proceeding { description = "Check for damage around your building.", idDisaster = 3 };
                this.Insert<Proceeding>(pro29);

                var pro30 = new Proceeding { description = "Move away from buildings, street lights, power lines, and anything else that could fall.", idDisaster = 3 };
                this.Insert<Proceeding>(pro30);

                var pro31 = new Proceeding { description = "Seek shelter near a hill or in a wide open area.", idDisaster = 3 };
                this.Insert<Proceeding>(pro31);

                var pro32 = new Proceeding { description = "Wait a moment or two after the first quake before moving to any other place.", idDisaster = 3 };
                this.Insert<Proceeding>(pro32);


                /* Procedimentos de nevasca */
                var pro20 = new Proceeding { description = "Stay inside your car or tent.", idDisaster = 4 };
                this.Insert<Proceeding>(pro20);

                var pro21 = new Proceeding { description = "Keep warm and dry.", idDisaster = 4 };
                this.Insert<Proceeding>(pro21);

                var pro22 = new Proceeding { description = "Stay hydrated.", idDisaster = 4 };
                this.Insert<Proceeding>(pro22);

                var pro23 = new Proceeding { description = "Determine what to do when the blizzard is over.", idDisaster = 4 };
                this.Insert<Proceeding>(pro23);

                var pro24 = new Proceeding { description = "Stay indoors as much as possible.", idDisaster = 4 };
                this.Insert<Proceeding>(pro24);

                var pro25 = new Proceeding { description = "Have backup heat.", idDisaster = 4 };
                this.Insert<Proceeding>(pro25);

                /* PRIMEIROS SOCORROS */

                var faid1 = new FirstAid { title = "Drowning" };
                this.Insert<FirstAid>(faid1);

                var faid2 = new FirstAid { title = "Burns" };
                this.Insert<FirstAid>(faid2);

                var faid3 = new FirstAid { title = "Smoke inhalation" };
                this.Insert<FirstAid>(faid3);

                var faid4 = new FirstAid { title = "Fainting" };
                this.Insert<FirstAid>(faid4);

                var faid5 = new FirstAid { title = "Cardiac Arrest" };
                this.Insert<FirstAid>(faid5);

                var faid6 = new FirstAid { title = "Light Wounds" };
                this.Insert<FirstAid>(faid6);

                var faid7 = new FirstAid { title = "Bleeding" };
                this.Insert<FirstAid>(faid7);

                /* PROCEDIMENTOS DE PRIMEIROS SOCORROS */

                /*AFOGAMENTO*/

                var prof1 = new ProceedingFirstAid { description = "Put the victim with the belly on the floor, with the head turned to one side (Safety lateral position)", idFirstAid = 1, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof1);

                var prof2 = new ProceedingFirstAid { description = "Calm down the victim: a) make the person rest; b) change wet clother; c) give hot beverages", idFirstAid = 1, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof2);

                var prof3 = new ProceedingFirstAid { description = "Press the torax 3 to 4 times to make the water comes out", idFirstAid = 1, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof3);

                var prof4 = new ProceedingFirstAid { description = "If the victim is not breathing, put the victm laying on their back and start the artificial (mouthtomouth) breathing.", idFirstAid = 1, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof4);

                var prof5 = new ProceedingFirstAid { description = "If the drowned person is unconscious: put on recovering position, keeping body in a safe and confortable position, impeding the tongue to block the throat and facilitate the liquids to come out.", idFirstAid = 1, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof5);

                /*QUEIMADURAS*/

                var prof6 = new ProceedingFirstAid { description = "Cool the area with cold running water for some minutes (as fast as you cool the area, less serious will be the burn)", idFirstAid = 2, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof6);

                var prof7 = new ProceedingFirstAid { description = "Remove rings or other metal objects that might also keep the heat", idFirstAid = 2, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof7);

                var prof8 = new ProceedingFirstAid { description = "Protect the area with clean cloth", idFirstAid = 2, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof8);

                var prof9 = new ProceedingFirstAid { description = "Take the victim to have medical care", idFirstAid = 2, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof9);

                /*INALAÇÃO DE FUMAÇA*/

                var prof10 = new ProceedingFirstAid { description = "Wet a clean cloth and tie on his face, like a mask, to avoid breathing the smoke.", idFirstAid = 3, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof10);

                var prof11 = new ProceedingFirstAid { description = "If there is a lot of smoke, stay down close to the ground where the heat is smaller and more oxygen;", idFirstAid = 3, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof11);

                var prof12 = new ProceedingFirstAid { description = "Remove the victim safely place of fire and lay down it on the floor;", idFirstAid = 3, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof12);

                var prof13 = new ProceedingFirstAid { description = "If the victim's body is on fire, roll it on the floor until they go out;", idFirstAid = 3, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof13);

                var prof14 = new ProceedingFirstAid { description = "Make sure the victim is breathing and the heart is beating;", idFirstAid = 3, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof14);

                /*DESMAIO*/

                var prof15 = new ProceedingFirstAid { description = "Holding the victim, preventing falls, without support on the ground;", idFirstAid = 4, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof15);

                var prof16 = new ProceedingFirstAid { description = "Lying Down slowly the victim on the floor with his head turned to the side;", idFirstAid = 4, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof16);

                var prof17 = new ProceedingFirstAid { description = "Raise the victim's legs to facilitate the return of blood to the heart and brain;", idFirstAid = 4, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof17);

                var prof18 = new ProceedingFirstAid { description = "Loosen clothing and keep the area ventilated environment", idFirstAid = 4, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof18);

                var prof19 = new ProceedingFirstAid { description = "After regaining consciousness, must remain at least ten minutes sitting, to avoid another fainting.", idFirstAid = 4, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof19);

                /* PARADA CARDÍACA*/

                var prof20 = new ProceedingFirstAid { description = "Pour the victim on the ground, belly up;", idFirstAid = 5, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof20);

                var prof21 = new ProceedingFirstAid { description = "Slightly lift the chin up person, to facilitate breathing;", idFirstAid = 5, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof21);

                var prof22 = new ProceedingFirstAid { description = "Support your hands, one on top of the other on the victim's chest, between the nipples, on top of the heart", idFirstAid = 5, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof22);

                var prof23 = new ProceedingFirstAid { description = "Make 2 compressions per second until the victim's heart beating on its own again, or until the ambulance arrived.", idFirstAid = 5, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof23);

                /* CORTES */

                var prof24 = new ProceedingFirstAid { description = "Wash hands with soap and water before making the curative;", idFirstAid = 6, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof24);

                var prof25 = new ProceedingFirstAid { description = "Wash the affected part, too, with soap and water, removing all of the injury locally and any dirt, such as land, grease, shards of glass, etc;", idFirstAid = 6, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof25);

                var prof26 = new ProceedingFirstAid { description = "Put an antiseptic", idFirstAid = 6, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof26);

                var prof27 = new ProceedingFirstAid { description = "Cover the wound with sterile gauze and tape, or clean cloth;", idFirstAid = 6, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof27);

                var prof28 = new ProceedingFirstAid { description = "Look for a health center", idFirstAid = 6, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof28);

                /* SANGRAMENTOS */

                var prof29 = new ProceedingFirstAid { description = "Elevate and thus keep the achieved member;", idFirstAid = 7, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof29);

                var prof30 = new ProceedingFirstAid { description = "Compress the area with sterile gauze or clean cloth;", idFirstAid = 7, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof30);

                var prof31 = new ProceedingFirstAid { description = "If compression is not enough to stop the bleeding, apply a tourniquet;", idFirstAid = 7, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof31);

                var prof32 = new ProceedingFirstAid { description = "Look for a health center", idFirstAid = 7, isChecked = false };
                this.Insert<ProceedingFirstAid>(prof32);
            }
        }

        public void Insert<T>(T entity) where T : class, new()
        {
            GetTable<T>().InsertOnSubmit(entity);
            SubmitChanges();
        }

        public void Delete<T>(T entity) where T : class, new()
        {
            GetTable<T>().DeleteOnSubmit(entity);
            SubmitChanges();
        }

        public List<T> SelectAll<T>() where T : class, new()
        {
            return GetTable<T>().ToList();
        }

        public void Update()
        {
            SubmitChanges();
        }


    }
}
