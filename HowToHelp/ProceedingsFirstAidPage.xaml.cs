﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HowToHelp.Classes;

namespace HowToHelp
{
    public partial class ProceedingsFirstAidPage : PhoneApplicationPage
    {
        public ProceedingsFirstAidPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var firstAid = (int)PhoneApplicationService.Current.State["firstAid"];

            var lstProceedingsFirstAid = Database.Instance.ProceedingFirstAids.Where(u => u.idFirstAid == firstAid).ToList();
            longLstProceedingsFirstAid.ItemsSource = lstProceedingsFirstAid;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var aux = (CheckBox)sender;
            var proceeedingFirstAid = Database.Instance.ProceedingFirstAids.Where(u => u.idProceedingFirstAid == (int)aux.Tag).First();

            proceeedingFirstAid.isChecked = aux.IsChecked.Value;
            Database.Instance.Update();
        }
    }
}