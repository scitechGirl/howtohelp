﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HowToHelp.Classes;

namespace HowToHelp
{
    public partial class CheckListPage : PhoneApplicationPage
    {
        public CheckListPage()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            var disaster = (int)PhoneApplicationService.Current.State["disaster"];

            var lstProceedings = Database.Instance.Proceedings.Where(u => u.idDisaster == disaster).ToList();
            longLstProceedings.ItemsSource = lstProceedings;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var aux = (CheckBox)sender;
            var proceeeding = Database.Instance.Proceedings.Where(u => u.idProceeding == (int)aux.Tag).First();

            proceeeding.isChecked = aux.IsChecked.Value;
            Database.Instance.Update();
        }
    }
}