﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HowToHelp.Classes;

namespace HowToHelp
{
    public partial class ContactsAuthoritiesPage : PhoneApplicationPage
    {
        //pega o usuário que vem da página de cadastro
        User usuario = (User) PhoneApplicationService.Current.State["user"];
        int i = 1;

        public ContactsAuthoritiesPage()
        {
            InitializeComponent();
        }

        private void cadastraContato()
        {
            Authority autoridade = new Authority();
            autoridade.name = txtName.Text;
            autoridade.phoneNumber = txtPhone.Text;
            autoridade.idAuthority = autoridade.idAuthority;

            Database.Instance.Insert<Authority>(autoridade);
            MessageBoxResult result = MessageBox.Show("Your contact was saved! Going to next page", "Contact saved!", MessageBoxButton.OK);
            i++;
        }

        private void btnProx_Click_1(object sender, EventArgs e)
        {
            cadastraContato();

            if (i == 2)
            {
                txtName.Text = "fire department";
                txtPhone.Text = "";
            }
            else
            {
                var uri = new Uri("/MainPage.xaml", UriKind.Relative);
                NavigationService.Navigate(uri);
            }
        }
    }
}