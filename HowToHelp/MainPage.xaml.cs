﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HowToHelp.Resources;
using HowToHelp.Classes;
using Microsoft.Phone.Tasks;

namespace HowToHelp
{
    public partial class MainPage : PhoneApplicationPage
    {
        User usuario = new User();

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            User usuario = (User)PhoneApplicationService.Current.State["user"];
            String destino = "";

            List<Contact> contatos = Database.Instance.Contacts.Where(o => o.idUser == usuario.idUser).ToList();

            if (contatos.Count > 1)
            {
                foreach (Contact c in contatos)
                {
                    destino = destino + c.phone + ";";
                }
            }
            else
            {
                destino = contatos.First().phone;
            }

            SmsComposeTask SMS = new SmsComposeTask();
            SMS.To = destino;
            SMS.Body = txtEmergency.Text + " alarm! I'm " + txtSituation.Text + " at " + txtLocation.Text + ". - " + usuario.name;
            SMS.Show();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            longLstManual.SelectedItem = null;
            longLstFirstAid.SelectedItem = null;

            usuario = Database.Instance.Users.First();

            PhoneApplicationService.Current.State["user"] = usuario;

            if (NavigationService.CanGoBack)
            {
                NavigationService.RemoveBackEntry();
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var contact = Database.Instance.Authorities.Where(u => u.name.Equals("ambulance")).First();
            PhoneCallTask phoneCallTask = new PhoneCallTask();

            phoneCallTask.PhoneNumber = contact.phoneNumber;
            phoneCallTask.DisplayName = contact.name;

            phoneCallTask.Show();
        }



        private void Panorama_Loaded(object sender, RoutedEventArgs e)
        {
            //Disasters
            var lstDesastres = Database.Instance.SelectAll<Disaster>();   
            List<AlphaKeyGroup<Disaster>> DataSourceDisaster = AlphaKeyGroup<Disaster>.CreateGroups(lstDesastres,
            System.Threading.Thread.CurrentThread.CurrentUICulture,
            (Disaster s) => { return s.title; }, true);

            longLstManual.ItemsSource = DataSourceDisaster;

            //FirstAid
            var lstFirstAid = Database.Instance.SelectAll<FirstAid>();
            List<AlphaKeyGroup<FirstAid>> DataSourceFirstAid = AlphaKeyGroup<FirstAid>.CreateGroups(lstFirstAid,
            System.Threading.Thread.CurrentThread.CurrentUICulture,
            (FirstAid s) => { return s.title; }, true);

            longLstFirstAid.ItemsSource = DataSourceFirstAid;
        }

        private void LongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var aux = longLstManual.SelectedItem as Disaster;

            if(aux != null) {
                PhoneApplicationService.Current.State["disaster"] = aux.idDisaster;
                var uri = new Uri("/CheckListPage.xaml", UriKind.Relative);
                NavigationService.Navigate(uri);
            }
        }

        private void longLstFirstAid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var aux = longLstFirstAid.SelectedItem as FirstAid;
            if (aux != null)
            {
                PhoneApplicationService.Current.State["firstAid"] = aux.idFirstAid;
                var uri = new Uri("/ProceedingsFirstAidPage.xaml", UriKind.Relative);
                NavigationService.Navigate(uri);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var contact = Database.Instance.Authorities.Where(u => u.name.Equals("fire department")).First();
            PhoneCallTask phoneCallTask = new PhoneCallTask();

            phoneCallTask.PhoneNumber = contact.phoneNumber;
            phoneCallTask.DisplayName = contact.name;

            phoneCallTask.Show();
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}