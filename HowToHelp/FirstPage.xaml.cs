﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HowToHelp.Classes;

namespace HowToHelp
{
    // PRIMEIRA PÁGINA - CADASTRO DO USUÁRIO
    public partial class FirstPage : PhoneApplicationPage
    {
        List<User> usuarios = new List<User>();

        public FirstPage()
        {
            InitializeComponent();
        }

        //verifica se nenhum usuário foi cadastrado até o momento
        public void checaPrimeiroAcesso() {
            usuarios = Database.Instance.SelectAll<User>();

            //Se existe usuário, então vai para a página principal
            if (usuarios.Count != 0)
            {
                var uri = new Uri("/MainPage.xaml", UriKind.Relative);
                NavigationService.Navigate(uri);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            checaPrimeiroAcesso();
        }



        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            //Dados do novo usuário
            User usuario = new User();
            usuario.name = txtName.Text;
            usuario.phoneNumber = txtPhone.Text;
            usuario.city = txtCity.Text;

            //Cadastro do usuário
            Database.Instance.Insert<User>(usuario);

            PhoneApplicationService.Current.State["user"] = Database.Instance.Users.Where(o => o.name == usuario.name && o.city == usuario.city && o.phoneNumber == usuario.phoneNumber).First();

            //Vai para a página de cadastro dos contatos
            var uri = new Uri("/ContactsPage.xaml", UriKind.Relative);
            NavigationService.Navigate(uri);
            
        }
    }
}