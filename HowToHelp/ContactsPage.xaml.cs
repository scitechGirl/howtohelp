﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HowToHelp.Classes;

namespace HowToHelp
{
    public partial class ContactsPage : PhoneApplicationPage
    {
        //pega o usuário que vem da página de cadastro
        User usuario = (User) PhoneApplicationService.Current.State["user"];

        public ContactsPage()
        {
            InitializeComponent();
        }

        private void txtName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {


        }

        private void cadastraContato()
        {
            Contact contato = new Contact();
            contato.name = txtName.Text;
            contato.phone = txtPhone.Text;
            contato.idUser = usuario.idUser;

            Database.Instance.Insert<Contact>(contato);
        }

        private void btnProx_Click_1(object sender, EventArgs e)
        {
            cadastraContato();

            MessageBoxResult result = MessageBox.Show("Would you like to insert another contact?", "Contact saved!", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                txtName.Text = "";
                txtPhone.Text = "";
            }
            else { 
                var uri = new Uri("/ContactsAuthoritiesPage.xaml", UriKind.Relative);
                NavigationService.Navigate(uri);
            }
        }
    }
}